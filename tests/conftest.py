import io
import os
from zipfile import ZIP_DEFLATED, ZipFile

import pytest
import requests_mock

from xdcget.config import read_config, write_xdcget_ini
from xdcget.index import IndexEntry


class TestDataDir:
    def __init__(self, path):
        self.path = path
        assert path.exists() and path.is_dir()
        self._entry_counter = 0

    def read_bytes(self, name):
        p = self.path.joinpath(name)
        assert p.exists()
        return p.read_bytes()

    def newIndexEntry(self, name=None, org="my", tag_name="0.1"):
        if name is None:
            name = f"untitled{self._entry_counter}"
            self._entry_counter += 1

        return IndexEntry(
            app_id=f"{org}-{name}",
            tag_name=tag_name,
            url=f"https://repo.org/{org}-{name}/d/{name}-{tag_name}.xdc",
            date="2023-09-18T18:07:56Z",
            description=r"something short\nsomething longer longer",
            source_code_url=f"https://repo.org/{org}/{name}",
            name=name,
            category="tool",
        )


class ZipFactory:
    def __init__(self, testdata, zfdir):
        self.testdata = testdata
        self.zfdir = zfdir
        assert zfdir.exists() and zfdir.is_dir()

    def path(self, entry, icon=None):
        fn = self.zfdir.joinpath(entry.cache_relname)
        assert not fn.exists()
        content = self.byteio(entry, icon=icon).read()
        fn.write_bytes(content)
        return fn

    def byteio(self, entry, icon=None):
        if icon is None:
            icon = self.testdata.read_bytes("icon.png")
        manifest = "\n".join(
            [
                f'name = "{entry.name}"',
                f'source_code_url = "{entry.source_code_url}"',
                f'description = "{entry.description}"',
                f'category = "{entry.category}"',
            ]
        )
        xdc = io.BytesIO()
        with ZipFile(xdc, "w", compression=ZIP_DEFLATED) as fzip:
            fzip.writestr("manifest.toml", manifest)
            fzip.writestr("index.html", "<html/>")
            fzip.writestr("icon.png", icon)
        xdc.seek(0)
        return xdc


@pytest.fixture
def testdata(request):
    return TestDataDir(request.path.parent.joinpath("data"))


@pytest.fixture
def zipfactory(testdata, tmp_path):
    zfdir = tmp_path.joinpath("zipfiles")
    zfdir.mkdir(parents=True, exist_ok=True)
    return ZipFactory(testdata=testdata, zfdir=zfdir)


@pytest.fixture
def exportdir(tmp_path):
    path = tmp_path.joinpath("export")
    path.mkdir()
    return path


class RequestMocker:
    def __init__(self, mock, zipfactory):
        self.requests_mock = mock
        self.zipfactory = zipfactory
        self.testdata = zipfactory.testdata
        self.requests_mock.get(
            "https://api.github.com/rate_limit",
            json={
                "resources": {
                    "core": {
                        "limit": 5000,
                        "used": 44,
                        "remaining": 4956,
                        "reset": 1690891414,
                    }
                }
            },
        )

    def mock_source(self, url):
        url, owner, name = url.strip("/").rsplit("/", 2)
        gh_assets = [
            {
                "name": f"{name}.xdc",
                "created_at": "2023-07-05T20:30:48Z",
                "browser_download_url": f"https://mocker.com/attachments/{owner}/{name}.xdc",
            }
        ]
        if url.startswith("https://codeberg.org"):
            self.requests_mock.get(
                f"https://codeberg.org/api/v1/repos/{owner}/{name}/releases/latest",
                json={"tag_name": "v1.0.1", "assets": gh_assets},
            )
        elif url.startswith("https://github.com"):
            self.requests_mock.get(
                f"https://api.github.com/repos/{owner}/{name}/releases/latest",
                json={"tag_name": "v1.0.1", "assets": gh_assets},
            )

        entry = self.testdata.newIndexEntry(name="poll")
        mock_content = self.zipfactory.byteio(entry).read()
        mock_url = f"https://mocker.com/attachments/{owner}/{name}.xdc"
        self.requests_mock.get(mock_url, content=mock_content)
        print(f"mocked {mock_url} with string length={len(mock_content)}")


def pytest_addoption(parser):
    parser.addoption(
        "--online",
        action="store_true",
        help="run test online doing network access, if not provided, network"
        " connections will be mocked and tests will run offline.",
    )


@pytest.fixture
def requests_mocker(request, monkeypatch, zipfactory):
    for api in ["GITHUB", "CODEBERG"]:
        monkeypatch.setenv(f"XDCGET_{api}_USER", "fakeuser")
        monkeypatch.setenv(f"XDCGET_{api}_TOKEN", "faketoken")
    with requests_mock.Mocker() as m:
        yield RequestMocker(m, zipfactory)


class IniConfig:
    def __init__(self, path, mocker=None):
        self._path = path
        self._mocker = mocker
        self.export_dir = path.joinpath("export_dir")
        self.cache_dir = path.joinpath("cache_dir")
        self.xdcget_ini_path = path.joinpath("xdcget.ini")
        self._sources = []
        self._locks = []

    def add_source(self, app_id, source_code_url, category="tool", description=None):
        """Add a new app source to the xdcget configuration."""
        if description is None:
            description = "some desc\n  " + "X" * 41
        self._sources.append(
            f"""
[app:{app_id}]
source_code_url = {source_code_url}
description = {description}
category = {category}
"""
        )
        if self._mocker is not None:
            self._mocker.mock_source(source_code_url)

    def add_lock_entry(
        self,
        app_id,
        name,
        tag_name,
        url,
        date,
        cache_relname,
        category="tool",
        description=None,
        size=42,
    ):
        """Add a new (pseudo) lock file entry to the xdcget configuration."""
        if description is None:
            description = "some desc\nqlwkeqlwkelqwkelqwkelqkwleqkwleqkwle"
        self._locks.append(
            f"""
[{app_id}]
app_id = "{app_id}"
name = "{name}"
tag_name = "{tag_name}"
url = "{url}"
date = "{date}"
cache_relname = "{cache_relname}"
description = "{description!r}"
category = "{category!r}"
size = "{size}"
"""
        )

    def create(self):
        """Create .ini and .lock files according to added sources/lock entries."""
        kw = dict(sources="\n".join(self._sources)) if self._sources else {}

        write_xdcget_ini(
            self.xdcget_ini_path,
            export_dir=self.export_dir,
            cache_dir=self.cache_dir,
            **kw,
        )
        if self._locks:
            with self._path.joinpath("xdcget.lock").open("w") as f:
                f.write("\n".join(self._locks))

        os.chdir(self._path)
        return read_config(self.xdcget_ini_path)


@pytest.fixture(params=["online", "offline"])
def iniconfig(request, tmp_path):
    """Return helper object to create .ini/.lock files for xdcget.

    On the returned object you can call `add_source` and
    `add_lock_entry` multiple times before calling `create()`
    which will then write out .ini/.lock files accordingly
    and change the working directory to the directory containing the files.
    """
    if request.param == "online":
        if not request.config.getoption("--online"):
            pytest.skip("specify --online to run the test against online servers")
        mocker = None
    else:
        mocker = request.getfixturevalue("requests_mocker")
    return IniConfig(tmp_path, mocker=mocker)


@pytest.fixture
def config_example1(iniconfig):
    iniconfig.add_source(
        app_id="webxdc-checklist",
        source_code_url="https://github.com/webxdc/checklist",
        category="tool",
    )
    iniconfig.add_source(
        app_id="webxdc-poll",
        source_code_url="https://codeberg.org/webxdc/poll",
        category="tool",
    )
    return iniconfig.create()
