"""
Functionality to parse and write xdcget.ini and xdcget.lock files.
"""

import configparser
import functools
import os
import re
import time
from dataclasses import dataclass
from pathlib import Path
from typing import Optional

from .errors import (
    EmptyOrUnsetEnvVar,
    IncompleteCredentials,
    InvalidAppId,
    MissingAPI,
    MissingSection,
)


def write_initial_config(xdcget_ini, out):
    """Write out example xdcget.ini file."""
    assert not xdcget_ini.exists(), xdcget_ini
    write_xdcget_ini(
        xdcget_ini, export_dir=Path("export_dir"), cache_dir=Path("cache_dir")
    )
    out.green(f"created -- please inspect: {xdcget_ini}")


def write_xdcget_ini(
    path,
    export_dir,
    cache_dir,
    codeberg_user_env_var="XDCGET_CODEBERG_USER",
    codeberg_token_env_var="XDCGET_CODEBERG_TOKEN",
    github_user_env_var="XDCGET_GITHUB_USER",
    github_token_env_var="XDCGET_GITHUB_TOKEN",
    sources="""\
#[app:<projectname>]
#source_code_url =
##description =
""",
):
    """Write out xdcget.ini file using the provided settings."""

    def get_rel_or_abs(path, rootpath):
        """Return a relative path if possible, otherwise the original path."""
        path = path.absolute()
        rootpath = rootpath.absolute()
        if path.is_relative_to(rootpath.parent):
            return Path(path.relative_to(rootpath.parent))
        return path

    with path.open("w") as f:
        w = functools.partial(print, file=f)
        cache_dir = get_rel_or_abs(cache_dir, path)
        export_dir = get_rel_or_abs(export_dir, path)

        w("[xdcget]")
        w("# all paths can be relative to the directory of the xdcget.ini file")
        w(f"export_dir = {export_dir}")
        w(f"cache_dir = {cache_dir}")
        w()
        w("[api:codeberg]")
        w("root_url = https://codeberg.org")
        w("api_url = https://codeberg.org/api/v1/")
        w(f"user_env_var = {codeberg_user_env_var}")
        w(f"token_env_var = {codeberg_token_env_var}")
        w()
        w("[api:github]")
        w("root_url = https://github.com")
        w("api_url = https://api.github.com/")
        w(f"user_env_var = {github_user_env_var}")
        w(f"token_env_var = {github_token_env_var}")
        w()
        w(sources)


def read_config(xdcget_ini_path):
    """Return Config object by parsing `[xdcget]` settings,
    `[api:]` definition sections and `[app:*]` sections.
    """
    with xdcget_ini_path.open("r") as f:
        parser = configparser.ConfigParser()
        parser.read_file(f)
        api_defs = []
        app_defs = []
        cfg = None
        basedir = xdcget_ini_path.parent
        for key in sorted(parser.sections()):
            api_def = parser[key]
            if key.startswith("api:"):
                api_defs.append(parse_api_def(key, api_def))
            elif key.startswith("app:"):
                app_id = key[len("app:") :]
                validate_app_id(app_id)
                app_defs.append(SourceDef(app_id=app_id, **parser[key]))
            else:
                assert key == "xdcget"
                cfg = parser[key]

    if cfg is None:
        raise MissingSection("xdcget")

    return Config(
        xdcget_ini_path=xdcget_ini_path,
        export_dir=basedir.joinpath(cfg["export_dir"]),
        cache_dir=basedir.joinpath(cfg["cache_dir"]),
        api_defs=api_defs,
        source_defs=app_defs,
    )


def validate_app_id(app_id):
    """Ensure that the specified app_id matches constraints,
    raise InvalidAppId otherwise.
    """
    if app_id.lower() != app_id:
        raise InvalidAppId(f"{app_id!r} is not lowercase")
    rex = re.compile(r"^[a-z][a-z0-9\-]*$")
    if not rex.match(app_id) or app_id.strip("-") != app_id:
        raise InvalidAppId(f"{app_id!r} contains invalid characters")


@dataclass
class ApiDef:
    """Data object with URLs and authentication information for
    a remote API.
    """

    root_url: str
    api_url: str
    user: Optional[str]
    token: Optional[str]

    @property
    def auth(self) -> Optional[tuple[str, str]]:
        """If user and token are set, returns the authentication tuple.
        Otherwise None is returned, because that is what requests lib expect
        if there are no auth.
        """
        if self.user and self.token:
            return self.user, self.token
        return None

    def serves(self, api_def):
        return api_def.source_code_url.startswith(self.root_url)

    def get_current_rate_limit(self, requests):
        if self.root_url == "https://github.com":
            r = requests.get("https://api.github.com/rate_limit", auth=self.auth)
            r.raise_for_status()
            cr = r.json()["resources"]["core"]
            return "gh api rate limits: {} out of {}, reset in {} secs".format(
                cr["used"], cr["limit"], cr["reset"] - time.time()
            )

    def get_latest_release_url(self, source_def):
        """Return the API endpoint for obtaining the "latest release" info for
        the given app source.
        """
        sc = source_def.source_code_url
        assert sc.startswith(self.root_url)
        repo_and_name = sc[len(self.root_url) :].strip("/")
        if self.root_url == "https://codeberg.org":
            return f"https://codeberg.org/api/v1/repos/{repo_and_name}/releases/latest"
        elif self.root_url == "https://github.com":
            return f"https://api.github.com/repos/{repo_and_name}/releases/latest"


@dataclass
class SourceDef:
    """Data object parsed from an `[app:]` section in the config file."""

    app_id: str
    source_code_url: str
    category: str
    description: Optional[str] = ""


class Config:
    """Config object providing access to parsed contents of the `xdcget.ini` file."""

    def __init__(self, xdcget_ini_path, export_dir, cache_dir, api_defs, source_defs):
        self.xdcget_ini_path = xdcget_ini_path
        basedir = xdcget_ini_path.parent
        self.export_dir = basedir.joinpath(export_dir).absolute()
        self.cache_dir = basedir.joinpath(cache_dir).absolute()
        self.api_defs = api_defs
        self.source_defs = source_defs

    @property
    def index_path(self):
        """Path to the xdcget.lock file containing Index information."""
        return self.xdcget_ini_path.parent.joinpath("xdcget.lock").absolute()

    def list_apps(self):
        """Return list of (SourceDef, ApiDef) instances for each configured app.

        Raise a MissingAPI Exception if any source does not have a matching API.
        """
        source_list = []
        for source_def in self.source_defs:
            for api_def in self.api_defs:
                if api_def.serves(source_def):
                    source_list.append((source_def, api_def))
                    break
            else:
                raise MissingAPI(source_def.app_id)

        return source_list

    def get_app(self, app_id):
        """Return (SourceDef, ApiDef) instances for the given app_id
        or None, if it doesn't exist.
        """
        for source_def, api_def in self.list_apps():
            if source_def.app_id == app_id:
                return source_def, api_def


def parse_api_def(section: str, api_def: dict) -> ApiDef:
    """Ensure that the given api_def definition is correct,
    and returns the corresponding ApiDef instance.

    Raises EmptyOrUnsetEnvVar if user_env_var or token_env_var is set but the
    environment variable is empty.
    Raises IncompleteCredentials if only one of user_env_var or token_env_var is set.
    """

    def _get_env_field_value(field, provided_field: str) -> str:
        env_var = api_def.get(field)
        if not env_var:
            raise IncompleteCredentials(
                section=section,
                provided=provided_field,
                missing=field,
            )
        val = os.environ.get(env_var, "")
        if not val:
            raise EmptyOrUnsetEnvVar(section=section, var=env_var)
        return val

    user, token = None, None
    if api_def.get("user_env_var") or api_def.get("token_env_var"):
        user = _get_env_field_value("user_env_var", "token_env_var")
        token = _get_env_field_value("token_env_var", "user_env_var")
    return ApiDef(api_def["root_url"], api_def["api_url"], user=user, token=token)
